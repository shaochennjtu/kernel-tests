# storage/ssd/nvme_setting_nr_request_bz1397927

Storage:Setting nr_request on NVMe device may make node hung 

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
#### You need run test_dev_setup to define TEST_DEVS
```bash
bash ./runtest.sh
```
